/*************************************************************************
 * FlashProg ATmega8A firmware.
 *
 * Copyright (C) 2015 Dilshan R Jayakody (4S6DRJ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/

#include "flashprog.h"
#include "usbdrv.h"

int main(void)
{
	InitSystem();
	
	while(1)
	{
		// update USB driver status.
		usbPoll();
		_delay_ms(1);
	}
}

/*****************************************************************************
 * Function: usbFunctionSetup
 *****************************************************************************
 *
 * Support function for LibUSB. This function handle all control messages 
 * received to endpoint 0 from host.
 *
 * @data: A pointer to the 8 bytes of setup data.
 *
 * return: Number of data blocks to send to the host.
 ****************************************************************************/
uchar usbFunctionSetup(uchar data[8])
{
	usbRequest_t *rq = (void *)data;
	
	// USB global response buffer is set to maximum of 64bits.
	static unsigned char responseBuffer[64];
	unsigned char responseBufferSize = 0;
	
	// Setup reference to latest USB command.
	currentInst = rq->bRequest;
	
	switch(rq->bRequest)
	{
		case DEVCMD_FLASHMEM_ID:
			// Read flash memory device IDs and push contents to USB output buffer.
			responseBufferSize = GetFlashMemID(responseBuffer, DEVCMD_FLASHMEM_ID);
			usbMsgPtr = (usbMsgPtr_t)responseBuffer;
			return responseBufferSize;
			break;
		case DEVCMD_SET_PARAM:
		case DEVCMD_SET_STATUS:
			// Set start address and scan length values, or status register value.
			return USB_NO_MSG;
			break;
		case DEVCMD_READ_BLOCK:
			// Read block of memory and return to the USB output buffer.
			responseBufferSize = ReadFlashMemBlock(responseBuffer);
			usbMsgPtr = (usbMsgPtr_t)responseBuffer;
			return responseBufferSize;
			break;
		case DEVCMD_ERASE:
			// Erase entire flash memory.
			EraseFlashMem();
			_delay_us(50);
			// Send response to host with flash memory WIP status.
			responseBuffer[0] = DEVCMD_ERASE;
			responseBuffer[1] = GetFlashMemStatus();
			usbMsgPtr = (usbMsgPtr_t)responseBuffer;
			return 2;
			break;
		case DEVCMD_GET_STATUS:
			// Send latest flash memory status value to host.
			responseBuffer[0] = DEVCMD_GET_STATUS;
			responseBuffer[1] = GetFlashMemStatus();
			usbMsgPtr = (usbMsgPtr_t)responseBuffer;
			return 2;
			break;
		case DEVCMD_SET_BUFFER:
			// Update write buffer with incoming data from host.
			ClearDataBuffer();
			bufferSpace = PAGEBUFFER_SIZE;
			bufferPos = 0;
			return USB_NO_MSG;
			break;
		case DEVCMD_WRITE:
			// Write local buffer data to flash memory page buffer.
			WriteFlashMemPage();
			// Send response to host with flash memory WIP status.
			responseBuffer[0] = DEVCMD_WRITE;
			responseBuffer[1] = GetFlashMemStatus();
			usbMsgPtr = (usbMsgPtr_t)responseBuffer;
			return 2;
			break;
		case DEVCMD_ERASE_BLOCK:
		case DEVCMD_ERASE_SECTOR:
			// Erase flash memory block or sector related to the given address.
			EraseFlashMemSeg((currentInst == DEVCMD_ERASE_BLOCK) ? FLASHMEM_BE : FLASHMEM_SE);
			_delay_us(50);
			// Send response to host with flash memory WIP status.
			responseBuffer[0] = currentInst;
			responseBuffer[1] = GetFlashMemStatus();
			usbMsgPtr = (usbMsgPtr_t)responseBuffer;
			return 2;
			break;
		case DEVCMD_UPDATE_STATUS:
			// Update status register value of flash memory.
			UpdateStatusRegister();
			_delay_us(50);
			// Send response to host with flash memory WIP status.
			responseBuffer[0] = DEVCMD_UPDATE_STATUS;
			responseBuffer[1] = GetFlashMemStatus();
			usbMsgPtr = (usbMsgPtr_t)responseBuffer;
			return 2;
			break;
	}
	
	return 0;
}

/*****************************************************************************
 * Function: usbFunctionWrite
 *****************************************************************************
 *
 * Support function for LibUSB. This function handle all payload data 
 * received from the host.
 *
 * @data: A pointer to the data buffer.
 * @len: Size of the data chunk.
 *
 * return: Indicate status of the data collection.
 ****************************************************************************/
uchar usbFunctionWrite(uchar *data, uchar len)
{
	if(currentInst == DEVCMD_SET_PARAM)
	{
		// Current instruction is set to address setup mode.
		if(len >= 4)
		{
			// Update global start address and read/write length.
			startAddress = data[0] + ((unsigned long int)data[1] << 8) + ((unsigned long int)data[2] << 16);
			readLength = data[3];
		}
		
		return 1;
	}
	else if(currentInst == DEVCMD_SET_STATUS)
	{
		// Update flash memory status register value.
		if(len >= 1)
		{
			statusReg = data[0];
		}
		
		return 1;
	}
	else if(currentInst == DEVCMD_SET_BUFFER)
	{
		// Current instruction is set to update the write buffer.
		if(len > bufferSpace)
		{
			// Guard to avoid buffer overruns.
			len = bufferSpace;
		}
		
		bufferSpace -= len;
		
		// Update data buffer with received data.
		for(unsigned short inputBufferPos = 0; inputBufferPos < len; inputBufferPos++)
		{
			// Guard to avoid local buffer overruns.
			if(bufferPos > PAGEBUFFER_SIZE)
			{
				bufferPos = 0;
			}
			
			pageBuffer[bufferPos++] = data[inputBufferPos];
		}
		
		return (bufferSpace == 0);
	}
	
	return 1;
}

/*****************************************************************************
 * Function: EraseFlashMem
 *****************************************************************************
 *
 * Erase all the content of the flash memory by issuing CE instruction to the 
 * chip. 
 *
 * return: None.
 ****************************************************************************/
void EraseFlashMem()
{
	PORTB = 0x00;
	ExecSPIWrite(FLASHMEM_WREN);
	
	// Release flash memory.
	PORTB = 0x04;
	
	// Select flash memory and send chip erase instruction to flash memory.
	_delay_ms(10);
	PORTB = 0x00;
	ExecSPIWrite(FLASHMEM_CE);
	
	// Release flash memory.
	PORTB = 0x04;
}

/*****************************************************************************
 * Function: EraseFlashMemSeg
 *****************************************************************************
 *
 * Erase either block or sector of the flash memory chip. The related block 
 * or sector is determined by the startAddress value. 
 *
 * @eraseCode: Erase mode. Valid values are FLASHMEM_BE or FLASHMEM_SE.  
 *
 * return: None.
 ****************************************************************************/
void EraseFlashMemSeg(unsigned char eraseCode)
{
	PORTB = 0x00;
	ExecSPIWrite(FLASHMEM_WREN);
	
	// Release flash memory.
	PORTB = 0x04;
	
	// Select flash memory and send chip erase instruction to flash memory.
	_delay_ms(10);
	PORTB = 0x00;
	ExecSPIWrite(eraseCode);
	
	// Send block address to erase.
	ExecSPIWrite((startAddress >> 16) & 0xFF);
	ExecSPIWrite((startAddress >> 8) & 0xFF);
	ExecSPIWrite(startAddress & 0xFF);
	
	// Release flash memory.
	PORTB = 0x04;
}

/*****************************************************************************
 * Function: UpdateStatusRegister
 *****************************************************************************
 *
 * Send request to change the value of Status Register of the flash memory 
 * chip. Status register value is submitting by referring statusReg global 
 * variable. 
 *
 * return: None.
 ****************************************************************************/
void UpdateStatusRegister()
{
	PORTB = 0x00;
	ExecSPIWrite(FLASHMEM_WREN);
	
	// Release flash memory.
	PORTB = 0x04;
	
	// Select flash memory and send status register instruction to flash memory.
	_delay_ms(10);
	PORTB = 0x00;
	ExecSPIWrite(FLASHMEM_WRSR);
	
	// Send status register value to flash memory.
	ExecSPIWrite(statusReg);
	
	// Release flash memory.
	PORTB = 0x04;
}

/*****************************************************************************
 * Function: WriteFlashMemPage
 *****************************************************************************
 *
 * Write all the contents of local pageBuffer to the flash memory page. 
 * Location of the flash memory page is determined by referring variable 
 * startAddress.
 *
 * return: None.
 ****************************************************************************/
void WriteFlashMemPage()
{
	unsigned short inputBufferPos = 0;
	
	// Select flash memory and send WREN instruction.
	PORTB = 0x00;
	ExecSPIWrite(FLASHMEM_WREN);
	
	// Release flash memory.
	PORTB = 0x04;
	
	// Select flash memory and send page program instruction to flash memory.
	_delay_ms(10);
	PORTB = 0x00;
	
	ExecSPIWrite(FLASHMEM_PP);
	
	// Send starting address bytes AD1, AD2 and AD3.
	ExecSPIWrite((startAddress >> 16) & 0xFF);
	ExecSPIWrite((startAddress >> 8) & 0xFF);
	ExecSPIWrite(startAddress & 0xFF);
	
	// Send data byes to flash memory page buffer.
	for(inputBufferPos = 0; inputBufferPos < PAGEBUFFER_SIZE; inputBufferPos++)
	{
		ExecSPIWrite(pageBuffer[inputBufferPos]);
	}
	
	// Release flash memory.
	PORTB = 0x04;
	
	// Update to next starting address.
	startAddress += inputBufferPos;
}

/*****************************************************************************
 * Function: ReadFlashMemBlock
 *****************************************************************************
 *
 * Read specified amount of data from flash memory chip. The number of bytes 
 * to read is depending on the value of readLength variable.
 *
 * @dataBuffer: Pointer to fill the received data from flash memory chip.
 *
 * return: Number of bytes read from the flash memory chip.
 ****************************************************************************/
unsigned char ReadFlashMemBlock(unsigned char* dataBuffer)
{
	PORTB = 0x00;

	ExecSPIWrite(FLASHMEM_READ);
	
	// Send starting address bytes AD1, AD2 and AD3.
	ExecSPIWrite((startAddress >> 16) & 0xFF);
	ExecSPIWrite((startAddress >> 8) & 0xFF);
	ExecSPIWrite(startAddress & 0xFF);
	_delay_us(10);
	
	for(unsigned char readPos = 0; readPos < readLength; readPos++)
	{
		ExecSPIWrite(0x00);
		dataBuffer[readPos] = SPDR;
		_delay_us(10);
	}

	// Release flash memory and return data buffer size.
	PORTB = 0x04;
	return readLength;
}

/*****************************************************************************
 * Function: GetFlashMemStatus
 *****************************************************************************
 *
 * Retrieve current value of flash memory Status Register by sending RDSR 
 * instruction to the flash memory chip.
 *
 * return: Current value of flash memory Status Register.
 ****************************************************************************/
unsigned char GetFlashMemStatus()
{
	unsigned char memStatus = 0;
	
	PORTB = 0x00;
	ExecSPIWrite(FLASHMEM_RDSR);
	
	// obtain chip status from SPI data buffer.
	_delay_us(10);
	ExecSPIWrite(0x00);
	memStatus = SPDR;
	
	// Release flash memory and return chip status byte.
	PORTB = 0x04;
	return memStatus;
}

/*****************************************************************************
 * Function: GetFlashMemID
 *****************************************************************************
 *
 * Read manufacturer ID and device ID by sending RDID instruction to the 
 * flash memory chip.
 *
 * @dataBuffer: Pointer to the data buffer to fill manufacture ID, device IDs 
 *              and command ID.
 *
 * @commandID:  USB request command ID related to the function call.
 *
 * return: Size of the output data buffer.
 ****************************************************************************/
unsigned char GetFlashMemID(unsigned char* dataBuffer, unsigned char commandID)
{
	PORTB = 0x00;
	dataBuffer[0] = commandID;
	
	ExecSPIWrite(FLASHMEM_RDID);
	_delay_us(10);
	
	// Retrieve device identification bytes from flash memory.
	for(unsigned char scanIndex = 0; scanIndex < 3; scanIndex++)
	{
		ExecSPIWrite(0x00);
		
		dataBuffer[scanIndex + 1] = SPDR;
		_delay_us(50);
	}
	
	// Release flash memory and return data buffer size.
	PORTB = 0x04;
	return 4;
}

/*****************************************************************************
 * Function: ExecSPIWrite
 *****************************************************************************
 *
 * Send specified command byte to the flash memory chip through SPI channel.
 *
 * @commandByte: SPI instruction or data to send to the flash memory chip.
 *
 * return: None.
 ****************************************************************************/
void ExecSPIWrite(unsigned char commandByte)
{
	unsigned char spiTimeout = 0;
	
	SPDR = commandByte;
	
	while((!(SPSR & (1 << SPIF))) && (spiTimeout < 100))
	{
		spiTimeout++;
	}
}

/*****************************************************************************
 * Function: ClearDataBuffer
 *****************************************************************************
 *
 * Reset all the content of input data buffer space to 0xFF.
 *
 * return: None.
 ****************************************************************************/
void ClearDataBuffer()
{
	for(unsigned short inputBufferPos = 0; inputBufferPos < PAGEBUFFER_SIZE; inputBufferPos++)
	{
		pageBuffer[inputBufferPos] = 0xFF;
	}
}

/*****************************************************************************
 * Function: InitSystem
 *****************************************************************************
 *
 * Initialize Atmega8 and V-USB sub system.
 *
 * return: None.
 ****************************************************************************/
void InitSystem()
{
	// Initialize USB driver.
	usbInit();
	usbDeviceDisconnect();
	_delay_ms(250);
	usbDeviceConnect();
	
	// Initialize MCU peripherals.
	sei();
	InitSPIMasterMode();
}

/*****************************************************************************
 * Function: InitSPIMasterMode
 *****************************************************************************
 *
 * Initialize Atmega8 SPI interface into master mode and setup clock rate 
 * to FCK/16 and SPI mode to 1.
 *
 * return: None.
 ****************************************************************************/
void InitSPIMasterMode()
{
	// Set MOSI, SCK and SS as output.
	DDRB = 0x2C;
	// Clear SS signal.
	PORTB = 0x04;
	// Enable SPI in master mode, set clock rate to FCK/16 and set SPI mode to 1.
	SPCR = (1 << SPE) | (1 << MSTR) | (1 << SPR0);
}