/*************************************************************************
 * FlashProg - programmer utility for low voltage SPI flash 
 *             memory devices.
 *
 * Copyright (C) 2015 Dilshan R Jayakody (4S6DRJ).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *************************************************************************/

#ifndef FLASHPROG_H_
#define FLASHPROG_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Platform specific routines.
#ifdef _WIN32
#include <lusb0_usb.h>

#define sleep(t) Sleep(t)
#else
#include <usb.h>

#define sleep(t) usleep(t)
#endif

// VID and PID of USB flash programmer device.
#define USBPROG_VID 0x1984
#define USBPROG_PID 0x0034

// USB programmer parameters.
#define USBPROG_TIMEOUT 4000
#define USBPROG_BUFFER_SIZE 64
#define USBPROG_WRITE_BUFFER_SIZE 256

// USB control requests.
#define DEVREQ_FLASHMEM_ID 0x01
#define DEVREQ_SET_PARAM 0x02
#define DEVREQ_READ_BLOCK 0x03
#define DEVREQ_ERASE 0x04
#define DEVREQ_GET_STATUS 0x05
#define DEVREQ_SET_BUFFER 0x06
#define DEVREQ_WRITE 0x07
#define DEVREQ_ERASE_BLOCK 0x08
#define DEVREQ_ERASE_SECTOR 0x09
#define DEVREQ_SET_STATUS 0x0A
#define DEVREQ_UPDATE_STATUS 0x0B

void GetFlashMemoryDeviceID();
unsigned char EraseFlashMemory();
unsigned char GetFlashMemoryStatus();
void SetFlashWriteAddress(unsigned int startAddr);
unsigned char EraseFlashMemorySegment(unsigned char eraseCommand);
unsigned char SetupStatusRegisterValue(unsigned char statusRegVal);
unsigned char ReadFlashMemoryBlock(unsigned int startAddr, unsigned char addrRange, unsigned char** dataBuffer);
unsigned char WriteFlashMemoryPage(unsigned char* dataBuffer, int dataBufferSize);

usb_dev_handle* OpenUsbFlashProg();
void CloseUsbFlashProg(usb_dev_handle* devHandle);

void ShowApplicationHelp();
void ShowApplicationVersion();

char animSeq[5] = "|/-\\";

#endif /* FLASHPROG_H_ */
